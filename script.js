let currenttime;

function datetime() {
  let dt = new Date();
  let dd = dt.toDateString() + " " + dt.toLocaleTimeString();
  document.getElementById("dateTime").innerHTML = dd;
}

setInterval(datetime, 1000);

function button() {
  var status = document.getElementById("button").value;
  if (status == "Start") {
    currenttime = new Date();

    let current = currenttime.toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
    });
    // console.log(currenttime);

    document.getElementById("starts").innerHTML = current;

    document.getElementById("button").value = "stop";
    document.getElementById("button").innerHTML = "Stop";
  } else if (status == "stop") {
    let endtime = new Date();

    let end = endtime.toLocaleTimeString("en-us", {
      hour: "2-digit",
      minute: "2-digit",
    });

    // console.log(endtime);

    // console.log(endtime - currenttime);

    document.getElementById("stops").innerHTML = end;
    // let total_minute = Math.floor((endtime.getTime() - currenttime.getTime())/60000);

    // console.log("Minute : ", Math.floor(total_minute/60000));

    let total_minute = 370;

    document.getElementById("button").innerHTML = "clear";
    document.getElementById("button").style.background = "gray";
    document.getElementById("button").value = "clear";
   
    document.getElementById("minute").innerHTML = total_minute;

    let over = parseInt(total_minute / 60);
    let modulus = total_minute % 60;
    let overPrice = over * 1500;

    // let modulus = Math.floor(total_hour/60000);
    // console.log(modulus);
    // document.getElementById("minute").innerHTML = modulus;

    // let overPrice;
    // let Price;

    if (modulus >= 0 && modulus <= 15) {
      var Price = 500;
    } else if (modulus > 15 && modulus <= 30) {
      var Price = 1000;
    } else if (modulus > 30 && modulus <= 60) {
      var Price = 1500;
    }
    let a = Math.floor(modulus / 60);
    Price += 1500 * a;
    document.getElementById("riel").innerHTML = Price;

    var add = overPrice + Price;
    console.log(add);
    console.log(overPrice);
    console.log(Price);
    console.log(a);

    document.getElementById("riel").innerHTML = "&nbsp;" + add;
  } else {
    document.getElementById("starts").innerHTML = "00:00";
    document.getElementById("stops").innerHTML = "00:00";
    document.getElementById("minute").innerHTML = "00:00";
    document.getElementById("riel").innerHTML = "00:00";
    document.getElementById("button").innerHTML = "start";
    document.getElementById("button").style.background = "pink";
    document.getElementById("button").value = "Start";
  }
}
